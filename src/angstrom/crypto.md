## Relatively simple algorithm

Comme l'acronyme le suggère ce challenge concernait RSA. 
Il s'agissait d'une application directe de l'algorithme de déchiffrement avec tous les paramètres connus.

La seule (petite) difficulté ici résistait à convertir le texte clair de sa version numérique en un clair littéral.

```python
>>> from math import *

>>> n = 113138904645172037883970365829067951997230612719077573521906183509830180342554841790268134999423971247602095979484887092205889453631416247856139838680189062511282674134361726455828113825651055263796576482555849771303361415911103661873954509376979834006775895197929252775133737380642752081153063469135950168223
>>> p = 11556895667671057477200219387242513875610589005594481832449286005570409920461121505578566298354611080750154513073654150580136639937876904687126793459819369
>>> q = 9789731420840260962289569924638041579833494812169162102854947552459243338614590024836083625245719375467053459789947717068410632082598060778090631475194567
>>> e = 65537
>>> c = 108644851584756918977851425216398363307810002101894230112870917234519516101802838576315116490794790271121303531868519534061050530562981420826020638383979983010271660175506402389504477695184339442431370630019572693659580322499801215041535132565595864123113626239232420183378765229045037108065155299178074809432

>>> dec = pow(c,d,n)
77829732531886017666421465369706368622254927240332446949265849761777437379574153694975519245766808162296991738636674224619780544798026515410227980157
>>> st=''
>>> for i in range(0,len(dec),2):
...     st+=chr(int(dec[i:i+2], 16))

>>> st
'actf{old_but_still_good_well_at_least_until_quantum_computing}'
```

## I'm so Random

On voit que chaque `Generator` est créé avec une _seed_ aléatoire que nous ne pourrons pas retrouver.

```python
r1 = Generator(random.randint(10000000, 99999999))
r2 = Generator(random.randint(10000000, 99999999))
```

En revanche, toutes les transformations effectuées sur la _seed_ pour sortir le nombre suivant sont déterministes donc si on détermine la valeur de la _seed_ à un moment, on peut calculer toutes les valeurs suivantes.

Le nombre aléatoire envoyé par l'application est en fait le produit de deux générateurs :
```python
r1.getNum() * r2.getNum()
```

Quand on obtient une valeur aléatoire, il faut donc trouver tous les facteurs qui peuvent être `r1` et `r2`. Une fois qu'on a cette liste, on calcule ce qui pourrait être pour chaque paire la valeur suivante et on compare avec la valeur suivante réelle de l'application. S'il n'y qu'une correspondance -- ce qui a été mon cas -- on connait les _seeds_ actuelles et on n'a plus qu'à calculer et soumettre la valeur suivante.

```python
tmp_factors = get_factors(int(input("first number")))
factors = []
for x,y in tmp_factors:
    factors.append((x,y))
    factors.append((y,x))

expected = []
